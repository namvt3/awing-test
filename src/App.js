import { useState } from "react";
import "./App.css";
import Content from "./components/content";
import Header from "./components/header";

function App() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [subCampaignList, setSubCampaignList] = useState([
    {
      name: "Chiến dịch con 1",
      status: true,
      ads: [
        {
          name: "Quảng cáo 1",
          quantity: 0,
          selected: false,
        },
      ],
    },
  ]);
  const [validated, setValidated] = useState(false);

  return (
    <div className='App'>
      <Header
        name={name}
        description={description}
        subCampaignList={subCampaignList}
        setValidated={setValidated}
      />
      <Content
        name={name}
        setName={setName}
        description={description}
        setDescription={setDescription}
        subCampaignList={subCampaignList}
        setSubCampaignList={setSubCampaignList}
        validated={validated}
      />
    </div>
  );
}

export default App;
