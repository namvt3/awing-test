import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import { useState } from "react";
import Information from "./information";
import ChildCampaign from "./child-campaign";

export default function Content({
  name,
  setName,
  description,
  setDescription,
  subCampaignList,
  setSubCampaignList,
  validated,
}) {
  const [value, setValue] = useState(0);

  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  const renderForm = () => {
    if (value === 0) {
      return (
        <Information
          name={name}
          setName={setName}
          description={description}
          setDescription={setDescription}
          validated={validated}
        />
      );
    } else if (value === 1) {
      return (
        <ChildCampaign
          subCampaignList={subCampaignList}
          setSubCampaignList={setSubCampaignList}
          validated={validated}
        />
      );
    }
  };

  return (
    <Box padding={3}>
      <Box boxShadow={1} borderRadius={1}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs value={value} onChange={handleChange}>
            <Tab label='Thông tin' style={{ minWidth: "160px" }} />
            <Tab label='Chiến dịch con' style={{ minWidth: "160px" }} />
          </Tabs>
        </Box>
        <Box padding={2}>{renderForm()}</Box>
      </Box>
    </Box>
  );
}
