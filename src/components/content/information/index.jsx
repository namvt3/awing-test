import { Box, TextField } from "@mui/material";

export default function Information({
  name,
  setName,
  description,
  setDescription,
  validated,
}) {
  return (
    <Box padding={1}>
      <TextField
        fullWidth
        label={<span className='required'>Tên chiến dịch</span>}
        variant='standard'
        value={name}
        onChange={(e) => setName(e.target.value)}
        error={validated && !name}
        helperText={validated && !name && "Dữ liệu không hợp lệ"}
      />
      <TextField
        fullWidth
        label='Mô tả'
        variant='standard'
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        margin='normal'
      />
    </Box>
  );
}
