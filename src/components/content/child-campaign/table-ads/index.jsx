import AddIcon from "@mui/icons-material/Add";
import DeleteIcon from "@mui/icons-material/Delete";
import {
  Button,
  Checkbox,
  FormControlLabel,
  IconButton,
  Input,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
} from "@mui/material";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import "./style.scss";

export default function TableAds({
  subCampaignList,
  setSubCampaignList,
  active,
  validated,
}) {
  const [checkboxHeaderStatus, setCheckboxHeaderStatus] = useState("none");

  useEffect(() => {
    checkSelectedAll(subCampaignList);
    // eslint-disable-next-line
  }, [subCampaignList[active]]);

  const checkSelectedAll = (array) => {
    const total = array[active].ads.length;
    let count = 0;
    for (let i = 0; i < total; i++) {
      if (array[active].ads[i].selected) {
        count++;
      }
    }
    if (count === 0) {
      setCheckboxHeaderStatus("none");
    } else if (count === total) {
      setCheckboxHeaderStatus("all");
    } else {
      setCheckboxHeaderStatus("indeterminate");
    }
  };

  const handleClickSelectAllAds = () => {
    setCheckboxHeaderStatus(checkboxHeaderStatus === "all" ? "none" : "all");

    const updatedSubCampaigns = [...subCampaignList];
    updatedSubCampaigns[active].ads.forEach(
      (item) => (item.selected = checkboxHeaderStatus === "all" ? false : true)
    );

    setSubCampaignList(updatedSubCampaigns);
  };

  const handleDeleteSelectedAds = () => {
    const updatedSubCampaigns = [...subCampaignList];
    updatedSubCampaigns[active].ads = updatedSubCampaigns[active].ads.filter(
      (item) => !item.selected
    );
    setSubCampaignList(updatedSubCampaigns);
    setCheckboxHeaderStatus("none");
  };

  const isSelected = (adIndex) => subCampaignList[active].ads[adIndex].selected;

  const handleChangeAd = (e, adIndex, property) => {
    const updatedSubCampaigns = [...subCampaignList];
    const subCampaign = { ...updatedSubCampaigns[active] };
    const updatedAds = [...subCampaign.ads];
    const ad = { ...updatedAds[adIndex] };
    ad[property] = e.target.value;
    updatedAds[adIndex] = ad;
    subCampaign.ads = updatedAds;
    updatedSubCampaigns[active] = subCampaign;

    setSubCampaignList(updatedSubCampaigns);
  };

  const addNewAd = () => {
    const updatedSubCampaigns = [...subCampaignList];
    const subCampaign = { ...updatedSubCampaigns[active] };
    subCampaign.ads = [
      ...subCampaign.ads,
      {
        name: `Quảng cáo ${subCampaign.ads.length + 1}`,
        quantity: 0,
        selected: false,
      },
    ];
    updatedSubCampaigns[active] = subCampaign;

    setSubCampaignList(updatedSubCampaigns);
  };

  const deleteAd = (adIndex) => {
    const updatedSubCampaigns = [...subCampaignList];
    const subCampaign = { ...updatedSubCampaigns[active] };
    subCampaign.ads.splice(adIndex, 1);
    updatedSubCampaigns[active] = subCampaign;

    setSubCampaignList(updatedSubCampaigns);
  };

  const handleSelectAd = (adIndex) => {
    const updatedSubCampaigns = [...subCampaignList];
    const subCampaign = { ...updatedSubCampaigns[active] };
    subCampaign.ads[adIndex].selected = !subCampaign.ads[adIndex].selected;
    updatedSubCampaigns[active] = subCampaign;

    setSubCampaignList(updatedSubCampaigns);
  };

  return (
    <TableContainer>
      <Table size='small'>
        <TableHead>
          <TableRow>
            <TableCell width='12px'>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={checkboxHeaderStatus === "all"}
                    onChange={handleClickSelectAllAds}
                    indeterminate={checkboxHeaderStatus === "indeterminate"}
                    color={checkboxHeaderStatus === "indeterminate" ? "default" : "info"}
                  />
                }
              />
            </TableCell>
            {checkboxHeaderStatus === "none" ? (
              <>
                <TableCell>
                  <Typography className='required'>Tên quảng cáo</Typography>
                </TableCell>
                <TableCell>
                  <Typography className='required'>Số lượng</Typography>
                </TableCell>
              </>
            ) : (
              <TableCell colSpan={2}>
                <Tooltip title='Xóa'>
                  <IconButton size='small' onClick={handleDeleteSelectedAds}>
                    <DeleteIcon fontSize='small' />
                  </IconButton>
                </Tooltip>
              </TableCell>
            )}

            <TableCell align='right'>
              <Button variant='outlined' onClick={addNewAd}>
                <span
                  style={{
                    display: "inherit",
                    marginLeft: "-4px",
                    marginRight: "8px",
                  }}
                >
                  <AddIcon />
                </span>
                Thêm
              </Button>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {subCampaignList[active].ads.map((item, index) => (
            <TableRow key={index} hover selected={item.selected} className='table-row'>
              <TableCell>
                <FormControlLabel
                  control={<Checkbox />}
                  checked={isSelected(index)}
                  onChange={() => handleSelectAd(index)}
                />
              </TableCell>
              <TableCell>
                <Input
                  fullWidth
                  value={item.name}
                  onChange={(e) => handleChangeAd(e, index, "name")}
                  variant='standard'
                  error={validated && !item.name}
                />
              </TableCell>
              <TableCell>
                <Input
                  fullWidth
                  value={item.quantity}
                  onChange={(e) => handleChangeAd(e, index, "quantity")}
                  variant='standard'
                  type='number'
                  error={validated && Number(item.quantity) < 1}
                />
              </TableCell>
              <TableCell align='right' width='120px'>
                <Tooltip title='Xóa'>
                  <IconButton
                    size='small'
                    onClick={() => deleteAd(index)}
                    disabled={checkboxHeaderStatus !== "none"}
                  >
                    <DeleteIcon fontSize='small' />
                  </IconButton>
                </Tooltip>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
