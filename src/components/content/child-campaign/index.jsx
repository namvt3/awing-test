import AddIcon from "@mui/icons-material/Add";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { Box, Checkbox, FormControlLabel, TextField } from "@mui/material";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import "./style.scss";
import TableAds from "./table-ads";

export default function ChildCampaign({
  subCampaignList,
  setSubCampaignList,
  validated,
}) {
  const [active, setActive] = useState(0);

  const addNewItem = () => {
    const length = subCampaignList.length;
    const newItem = {
      name: `Chiến dịch con ${length + 1}`,
      status: true,
      ads: [
        {
          name: "Quảng cáo 1",
          quantity: 0,
          selected: false,
        },
      ],
    };
    setSubCampaignList((state) => [...state, newItem]);
    setActive(length);
  };

  const checkError = (subCampaign) => {
    let result = false;
    if (!subCampaign.name || !subCampaign.ads.length || checkErrorAds(subCampaign.ads)) {
      result = true;
    }
    return result;
  };

  const checkErrorAds = (adList) => {
    let i = 0;
    let flag = false;
    while (i < adList.length && !flag) {
      if (!adList[i].name || Number(adList[i].quantity < 1)) {
        flag = true;
        return true;
      } else {
        i++;
      }
    }
    return false;
  };

  const total = (item) => {
    let totalQuantity = 0;
    for (const ad of item.ads) {
      totalQuantity += Number(ad.quantity);
    }
    return totalQuantity;
  };

  const handleChangeName = (e) => {
    const newValue = e.target.value;
    const newArr = [...subCampaignList];
    newArr[active].name = newValue;
    setSubCampaignList(newArr);
  };

  const handleChangeStatus = () => {
    const newArr = [...subCampaignList];
    newArr[active].status = !newArr[active].status;
    setSubCampaignList(newArr);
  };

  return (
    <Box className='child-campaign'>
      <Box display='flex' justifyContent='flex-start' className='scroll-horizontal'>
        <button className='button-add' onClick={addNewItem}>
          <AddIcon style={{ color: "red" }} />
        </button>

        {subCampaignList.map((item, index) => (
          <Box
            key={index}
            className={`card ${index === active ? "active" : ""}`}
            boxShadow={1}
            onClick={() => setActive(index)}
            flexShrink={0}
          >
            <div className='card__header'>
              <span>
                <Typography
                  variant='h6'
                  gutterBottom
                  className={validated && checkError(item) ? "error" : ""}
                >
                  {item.name}
                  <CheckCircleIcon
                    style={{
                      color: item.status ? "green" : "gray",
                    }}
                    className='card__header__icon'
                  />
                </Typography>
              </span>
            </div>
            <div className='card__content'>
              <Typography variant='h5' gutterBottom>
                {total(item)}
              </Typography>
            </div>
          </Box>
        ))}
      </Box>

      <Box marginTop={2}>
        <Grid container padding='8px 8px 0'>
          <Grid item xs={8} padding={1}>
            <TextField
              fullWidth
              label={<span className='required'>Tên chiến dịch con</span>}
              variant='standard'
              value={subCampaignList[active].name}
              onChange={handleChangeName}
              error={validated && !subCampaignList[active].name}
              helperText={
                validated && !subCampaignList[active].name && "Dữ liệu không hợp lệ"
              }
            />
          </Grid>
          <Grid
            item
            xs={4}
            padding={0.5}
            display='flex'
            alignItems='center'
            justifyContent='center'
          >
            <FormControlLabel
              control={<Checkbox />}
              label='Đang hoạt động'
              checked={subCampaignList[active].status}
              onChange={handleChangeStatus}
            />
          </Grid>
        </Grid>

        <Typography
          variant='h6'
          gutterBottom
          textTransform='uppercase'
          textAlign='left'
          marginTop={2}
          padding={2}
        >
          Danh sách quảng cáo
        </Typography>

        <TableAds
          subCampaignList={subCampaignList}
          setSubCampaignList={setSubCampaignList}
          active={active}
          validated={validated}
        />
      </Box>
    </Box>
  );
}
