import { Alert, Box, Button, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import "./style.scss";

export default function Header({ name, description, subCampaignList, setValidated }) {
  const [open, setOpen] = useState("none");
  const [data, setData] = useState({});

  const checkErrorSubCampaignList = () => {
    for (let i = 0; i < subCampaignList.length; i++) {
      if (!subCampaignList[i].name) {
        return true;
      }

      if (!subCampaignList[i].ads.length) {
        return true;
      }

      for (let j = 0; j < subCampaignList[i].ads.length; j++) {
        if (
          !subCampaignList[i].ads[j].name ||
          parseInt(subCampaignList[i].ads[j].quantity) < 1
        ) {
          return true;
        }
      }
    }

    return false;
  };

  const handleSubmit = () => {
    setValidated(true);

    if (!name || checkErrorSubCampaignList()) {
      setOpen("error");
    } else {
      const deletedSelectedProperty = subCampaignList.map((obj) => {
        const newObj = { ...obj };
        newObj.ads = obj.ads.map((ad) => {
          const newAd = { ...ad };
          delete newAd.selected;
          return newAd;
        });
        return newObj;
      });

      const object = {
        information: {
          name: name,
          describe: description,
        },
        subCampaigns: deletedSelectedProperty,
      };

      setData(object);
      setOpen("success");
    }
  };

  return (
    <Box paddingTop={2.5}>
      {open !== "none" && (
        <Alert severity={open} onClose={() => setOpen("none")} className='alert'>
          {open === "success"
            ? `Thêm thành công chiến dịch ${JSON.stringify({ campaign: data })}`
            : "Vui lòng điền đúng và đầy đủ thông tin"}
        </Alert>
      )}
      <Box
        display='flex'
        justifyContent='space-between'
        padding='10px 20px'
        borderBottom={1}
      >
        <Typography>VŨ TIẾN NAM</Typography>
        <Button variant='contained' onClick={handleSubmit}>
          SUBMIT
        </Button>
      </Box>
    </Box>
  );
}
